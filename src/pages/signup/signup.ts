import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';

import {MainPage} from '../pages';
import {AuthService} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { name: string, email: string, password: string } = {
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public translateService: TranslateService,
              private auth: AuthService) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  doSignup() {
    this.auth.signUp(this.account).then(
      () => this.navCtrl.push(MainPage),
      error => {
        console.log(error);
        // Unable to sign up
        let toast = this.toastCtrl.create({
          message: this.signupErrorString + error.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    );
  }
}
